INSERT INTO state(name)
VALUES ('Santa Catarina');

INSERT INTO city(name, state_id)
VALUES ('Joinville', (SELECT id FROM state WHERE name = 'Santa Catarina'));

INSERT INTO neighborhood(name, city_id)
VALUES ('Costa e Silva', (SELECT id FROM city WHERE name = 'Joinville'));

INSERT INTO client(name, neighborhood_id)
VALUES ('Client 1', (SELECT id FROM neighborhood WHERE name = 'Costa e Silva')),
       ('Client 2', (SELECT id FROM neighborhood WHERE name = 'Costa e Silva')),
       ('Client 3', (SELECT id FROM neighborhood WHERE name = 'Costa e Silva'));

INSERT INTO supplier(name)
VALUES ('Supplier 1'),
       ('Supplier 2'),
       ('Supplier 3');

INSERT INTO user_table(name, username, password, supplier_id)
VALUES ('User 1', 'user1', '$2a$10$fSerlN8Qt6YUJdjN8eqFJO2n7InoQrmbL2GqEPzX4b72.0YWFtNqS', (SELECT id FROM supplier WHERE name = 'Supplier 1')),
       ('User 2', 'user2', '$2a$10$jSM8ATiI.r06h3ojgiQkDOci8uE/HaqMKv1v5FMGQZpbJTOs8BrTW', (SELECT id FROM supplier WHERE name = 'Supplier 2')),
       ('User 3', 'user3', '$2a$10$Nm3H66PqxA9RJ0ihwEYAeODvqi98VEfqLTfK4cznHIHUaKIbtyyYa', (SELECT id FROM supplier WHERE name = 'Supplier 3'));

INSERT INTO job_type(name)
VALUES ('Conserto eletrônico'),
       ('serviços gerais'),
       ('manutenção hidráulica'),
       ('instalação elétrica'),
       ('jardinagem');
