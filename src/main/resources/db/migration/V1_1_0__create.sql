CREATE TABLE SPRING_SESSION
(
  PRIMARY_ID            CHAR(36) NOT NULL,
  SESSION_ID            CHAR(36) NOT NULL,
  CREATION_TIME         BIGINT   NOT NULL,
  LAST_ACCESS_TIME      BIGINT   NOT NULL,
  MAX_INACTIVE_INTERVAL INT      NOT NULL,
  EXPIRY_TIME           BIGINT   NOT NULL,
  PRINCIPAL_NAME        VARCHAR(100),
  CONSTRAINT SPRING_SESSION_PK PRIMARY KEY (PRIMARY_ID)
);

CREATE UNIQUE INDEX SPRING_SESSION_IX1 ON SPRING_SESSION (SESSION_ID);
CREATE INDEX SPRING_SESSION_IX2 ON SPRING_SESSION (EXPIRY_TIME);
CREATE INDEX SPRING_SESSION_IX3 ON SPRING_SESSION (PRINCIPAL_NAME);

CREATE TABLE SPRING_SESSION_ATTRIBUTES
(
  SESSION_PRIMARY_ID CHAR(36)     NOT NULL,
  ATTRIBUTE_NAME     VARCHAR(200) NOT NULL,
  ATTRIBUTE_BYTES    BYTEA        NOT NULL,
  CONSTRAINT SPRING_SESSION_ATTRIBUTES_PK PRIMARY KEY (SESSION_PRIMARY_ID, ATTRIBUTE_NAME),
  CONSTRAINT SPRING_SESSION_ATTRIBUTES_FK FOREIGN KEY (SESSION_PRIMARY_ID) REFERENCES SPRING_SESSION (PRIMARY_ID) ON DELETE CASCADE
);


CREATE TABLE state
(
  id   SERIAL                 NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT state_pk PRIMARY KEY (id)
);

CREATE TABLE city
(
  id       SERIAL                 NOT NULL,
  name     character varying(255) NOT NULL,
  state_id INTEGER                NOT NULL,
  CONSTRAINT city_pk PRIMARY KEY (id),
  CONSTRAINT state_city_fk FOREIGN KEY (state_id) REFERENCES state (id)
);

CREATE TABLE neighborhood
(
  id      SERIAL                 NOT NULL,
  name    character varying(255) NOT NULL,
  city_id INTEGER                NOT NULL,
  CONSTRAINT neighborhood_pk PRIMARY KEY (id),
  CONSTRAINT city_neighborhood_fk FOREIGN KEY (city_id) REFERENCES city (id)
);

CREATE TABLE client
(
  id              BIGSERIAL              NOT NULL,
  name            character varying(255) NOT NULL,
  neighborhood_id INTEGER                NOT NULL,
  CONSTRAINT client_pk PRIMARY KEY (id),
  CONSTRAINT neighborhood_client_fk FOREIGN KEY (neighborhood_id) REFERENCES neighborhood (id)
);

CREATE TABLE supplier
(
  id   BIGSERIAL              NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT supplier_pk PRIMARY KEY (id)
);

CREATE TABLE supplier_has_client
(
  supplier_id BIGINT NOT NULL,
  client_id   BIGINT NOT NULL,
  CONSTRAINT supplier_has_client_pk PRIMARY KEY (supplier_id, client_id),
  CONSTRAINT client_supplier_has_client_fk FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT supplier_supplier_has_client_fk FOREIGN KEY (supplier_id) REFERENCES supplier (id)
);

CREATE TABLE job_type
(
  id   SERIAL                 NOT NULL,
  name character varying(255) NOT NULL,
  CONSTRAINT job_type_pk PRIMARY KEY (id)
);

CREATE TABLE job
(
  id          BIGSERIAL      NOT NULL,
  client_id   BIGINT         NOT NULL,
  supplier_id BIGINT         NOT NULL,
  description TEXT           NOT NULL,
  date        DATE           NOT NULL,
  job_value   DECIMAL(10, 2) NOT NULL,
  job_type_id INTEGER        NOT NULL,
  CONSTRAINT job_pk PRIMARY KEY (id, client_id, supplier_id),
  CONSTRAINT client_job_fk FOREIGN KEY (client_id) REFERENCES client (id),
  CONSTRAINT supplier_job_fk FOREIGN KEY (supplier_id) REFERENCES supplier (id),
  CONSTRAINT job_type_job_fk FOREIGN KEY (job_type_id) REFERENCES job_type (id)

);

CREATE TABLE user_table
(
  id          BIGSERIAL              NOT NULL,
  name        character varying(255) NOT NULL,
  username    character varying(255) NOT NULL,
  password    character varying(255) NOT NULL,
  supplier_id BIGINT                 NOT NULL,
  CONSTRAINT user_table_pk PRIMARY KEY (id),
  CONSTRAINT supplier_user_table_fk FOREIGN KEY (supplier_id) REFERENCES supplier (id)
);

CREATE UNIQUE INDEX idx_user_table_name_unq ON user_table (name);