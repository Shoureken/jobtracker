package com.dematte.servicerecord.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.dematte.servicerecord.service")
public class ServiceConfig {



}
