package com.dematte.servicerecord.service;

import java.util.Collections;
import java.util.Optional;

import com.dematte.servicerecord.jpa.entity.User;
import com.dematte.servicerecord.jpa.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService implements UserDetailsService {

	private final UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		final Optional<User> optional = userRepository.findByUsername(login);
		return optional
				.map(UserService::getUserDetails)
				.orElseThrow(() -> new UsernameNotFoundException("User not found with login: " + login));
	}

	public User getByUsername(String login) {
		final Optional<User> optional = userRepository.findByUsername(login);
		return optional.get();
	}

	private static UserDetails getUserDetails(com.dematte.servicerecord.jpa.entity.User user) {
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(),
				user.getPassword(),
				Collections.emptyList()
		);
	}

}