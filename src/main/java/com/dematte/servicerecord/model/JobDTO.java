package com.dematte.servicerecord.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDTO {

	@Positive
	private Long id;

	@NotNull
	@Positive
	private Long clientId;
	private String description;

	@NotNull
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;

	@NotNull
	@Positive
	private BigDecimal jobValue;
	@NotNull
	@Positive
	private Integer jobTypeId;

}
