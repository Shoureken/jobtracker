package com.dematte.servicerecord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.dematte.servicerecord.config"})
public class ServiceRecordApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRecordApplication.class, args);
	}

}
