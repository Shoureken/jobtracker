package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.TemporalType.DATE;

@Data
@NoArgsConstructor
@Entity
public class Job {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "supplier_id", nullable = false)
	private Supplier supplier;

	@ManyToOne(optional = false)
	@JoinColumn(name = "client_id", nullable = false)
	private Client client;

	private String description;

	@Temporal(DATE)
	private Date date;

	@Column(name = "job_value")
	private BigDecimal value;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "job_type_id", nullable = false)
	private JobType jobType;

	public Job(Supplier supplier, Client client, String description, Date date, BigDecimal value, JobType jobType) {
		this.supplier = supplier;
		this.client = client;
		this.description = description;
		this.date = date;
		this.value = value;
		this.jobType = jobType;
	}

}
