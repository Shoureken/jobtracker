package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
public class State {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Integer id;
	private String name;

}
