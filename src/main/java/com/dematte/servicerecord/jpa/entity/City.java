package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
public class City {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Integer id;
	private String name;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "state_id", nullable = false)
	private State state;

}
