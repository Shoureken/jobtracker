package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
@Table(name = "user_table")
public class User {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	private String name;
	private String username;
	private String password;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "supplier_id", nullable = false)
	private Supplier supplier;

}
