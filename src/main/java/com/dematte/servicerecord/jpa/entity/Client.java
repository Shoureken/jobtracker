package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	private String name;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "neighborhood_id", nullable = false)
	private Neighborhood neighborhood;

	@ManyToMany(mappedBy = "clients")
	private List<Supplier> suppliers;

}
