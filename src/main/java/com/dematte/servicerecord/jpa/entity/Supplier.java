package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
public class Supplier {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Long id;
	private String name;

	@ManyToMany
	@JoinTable(
			name = "supplier_has_client",
			joinColumns = {@JoinColumn(name = "supplier_id")},
			inverseJoinColumns = {@JoinColumn(name = "client_id")}
	)
	private List<Client> clients;

}
