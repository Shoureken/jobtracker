package com.dematte.servicerecord.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@Entity
public class Neighborhood {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Integer id;
	private String name;

	@ManyToOne(fetch = EAGER)
	@JoinColumn(name = "city_id", nullable = false)
	private City city;

}
