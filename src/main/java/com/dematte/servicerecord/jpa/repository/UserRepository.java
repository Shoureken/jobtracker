package com.dematte.servicerecord.jpa.repository;

import java.util.Optional;

import com.dematte.servicerecord.jpa.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends RsqlRepository<User, Long> {

	Optional<User> findByUsername(String username);

}