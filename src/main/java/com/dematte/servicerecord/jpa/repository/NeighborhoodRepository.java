package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.Neighborhood;
import org.springframework.stereotype.Repository;

@Repository
public interface NeighborhoodRepository extends RsqlRepository<Neighborhood, Integer> {

}