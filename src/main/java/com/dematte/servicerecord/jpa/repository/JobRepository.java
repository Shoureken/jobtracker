package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.Job;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends RsqlRepository<Job, Long> {

}