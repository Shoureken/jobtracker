package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.Client;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends RsqlRepository<Client, Long> {

}