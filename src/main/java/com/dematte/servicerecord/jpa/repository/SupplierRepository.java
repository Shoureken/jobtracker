package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.Supplier;
import org.springframework.stereotype.Repository;

@Repository
public interface SupplierRepository extends RsqlRepository<Supplier, Long> {

}