package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.State;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends RsqlRepository<State, Integer> {

}