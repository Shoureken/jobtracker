package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.JobType;
import org.springframework.stereotype.Repository;

@Repository
public interface JobTypeRepository extends RsqlRepository<JobType, Integer> {

}