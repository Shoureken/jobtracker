package com.dematte.servicerecord.jpa.repository;

import com.dematte.servicerecord.jpa.entity.City;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends RsqlRepository<City, Integer> {

}