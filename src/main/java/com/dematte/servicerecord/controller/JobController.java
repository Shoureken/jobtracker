package com.dematte.servicerecord.controller;

import javax.validation.Valid;
import java.util.List;

import com.dematte.servicerecord.jpa.entity.Client;
import com.dematte.servicerecord.jpa.entity.Job;
import com.dematte.servicerecord.jpa.entity.JobType;
import com.dematte.servicerecord.jpa.entity.Supplier;
import com.dematte.servicerecord.jpa.entity.User;
import com.dematte.servicerecord.jpa.repository.ClientRepository;
import com.dematte.servicerecord.jpa.repository.JobRepository;
import com.dematte.servicerecord.jpa.repository.JobTypeRepository;
import com.dematte.servicerecord.model.JobDTO;
import com.dematte.servicerecord.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@RequiredArgsConstructor
@Controller
@RequestMapping("jobs")
public class JobController {

	private final JobRepository jobRepository;
	private final ClientRepository clientRepository;
	private final JobTypeRepository jobTypeRepository;
	private final UserService userService;

	@GetMapping
	public String index(Model model) {
		final List<Job> jobs = jobRepository.findAll();
		if (jobs != null && !jobs.isEmpty()) {
			model.addAttribute("jobs", jobs);
		}
		return "jobs-index";
	}

	@GetMapping("add")
	public String getAdd(Model model, JobDTO jobDTO) {
		model.addAttribute("job", jobDTO);
		model.addAttribute("clients", clientRepository.findAll());
		model.addAttribute("jobTypes", jobTypeRepository.findAll());
		return "jobs-add";
	}

	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") long id, Model model) {
		final Job job = jobRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid job Id:" + id));
		final JobDTO jobDTO = getJobDTO(job);
		return getAdd(model, jobDTO);
	}

	@PostMapping("add")
	public String addUser(@AuthenticationPrincipal org.springframework.security.core.userdetails.User user,
	                      @Valid JobDTO jobDTO,
	                      BindingResult result,
	                      Model model) {
		if (result.hasErrors()) {
			return getAdd(model, jobDTO);
		}
		final User systemUser = userService.getByUsername(user.getUsername());
		final Supplier supplier = systemUser.getSupplier();

		final Job job = getJob(jobDTO, supplier);
		jobRepository.save(job);
		return index(model);
	}

	private Job getJob(JobDTO jobDTO, Supplier supplier) {
		final Client client = clientRepository.getOne(jobDTO.getClientId());
		final JobType jobType = jobTypeRepository.getOne(jobDTO.getJobTypeId());

		final Long id = jobDTO.getId();
		if (id == null) {
			return new Job(supplier, client, jobDTO.getDescription(), jobDTO.getDate(), jobDTO.getJobValue(), jobType);
		}

		final Job job = jobRepository.getOne(id);
		job.setClient(client);
		job.setJobType(jobType);
		job.setDescription(jobDTO.getDescription());
		job.setDate(jobDTO.getDate());
		job.setSupplier(supplier);
		job.setValue(job.getValue());
		return job;
	}

	private JobDTO getJobDTO(Job job) {
		return new JobDTO(job.getId(), job.getClient().getId(), job.getDescription(), job.getDate(), job.getValue(), job.getJobType().getId());
	}

//	@PostMapping("/update/{id}")
//	public String updateUser(@PathVariable("id") long id, @Valid User user,
//	                         BindingResult result, Model model) {
//		if (result.hasErrors()) {
//			user.setId(id);
//			return "update-user";
//		}
//
////		jobRepository.save(user);
//		model.addAttribute("users", jobRepository.findAll());
//		return "index";
//	}

}